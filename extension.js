const vscode = require('vscode')

function activate(context) {
	vscode.window.tabGroups.onDidChangeTabs(async (tabEvent) => {
		const tabList = vscode.window.tabGroups.all.flatMap(tabGroup => tabGroup.tabs)
		if(tabList.length > 1) {
			vscode.commands.executeCommand('workbench.action.showMultipleEditorTabs')
		} else {
			vscode.commands.executeCommand('workbench.action.hideEditorTabs')
		}
	})
}

function deactivate() {
	vscode.commands.executeCommand('workbench.action.showMultipleEditorTabs')
}

module.exports = {
	activate,
	deactivate
}
