# tabautohide

## Features

Automatically show and hide the tab bar in VS Code based on number of tabs open. With a single tab, the tab bar is hidden, otherwise it is shown.

## Installing
Use `vsce package` to build a VS Code package for this extension. See [VS Code docs](https://code.visualstudio.com/api/working-with-extensions/publishing-extension#packaging-extensions)

## Releases

### 1.0.0

Initial release
